﻿using System.Collections.Generic;
using UnityEngine;


namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_Angles : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private UniformImage image;
        [SerializeField] private GameObject stickmanPrefab;
        [SerializeField] private GameObject stickmanAnglesPrefab;
        [SerializeField] private Transform canvas;

        private KinectSensor sensor;
        private List<Stickman> stickmen;
        private List<StickmanAngles> stickmenAngles;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();
        }

        private void Update()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                if (frame.ColorFrameSource != null)
                {
                    image.Load(frame.ColorFrameSource);
                }

                if (frame.BodyFrameSource != null)
                {
                    UpdateStickmen(frame.BodyFrameSource.Bodies);
                }
            }
        }

        private void OnDestroy()
        {
            sensor?.Close();
        }

        private void UpdateStickmen(List<Body> bodies)
        {
            if (bodies == null)
            {
                return;
            }

            if (stickmen == null)
            {
                stickmen = new List<Stickman>();
                stickmenAngles = new List<StickmanAngles>();
            }

            if (stickmen.Count != bodies.Count)
            {
                foreach (Stickman stickman in stickmen)
                {
                    Destroy(stickman.gameObject);
                }
                foreach (StickmanAngles item in stickmenAngles)
                {
                    Destroy(item.gameObject);
                }

                stickmen.Clear();
                stickmenAngles.Clear();

                foreach (Body body in bodies)
                {
                    Stickman stickman = Instantiate(stickmanPrefab, canvas).GetComponent<Stickman>();
                    stickmen.Add(stickman);

                    StickmanAngles stickmanAngles = Instantiate(stickmanAnglesPrefab, canvas).GetComponent<StickmanAngles>();
                    stickmanAngles.RegisterStickman(stickman);
                    stickmenAngles.Add(stickmanAngles);
                }
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                stickmen[i].UpdateStickman(bodies[i], image, Space.Color);
                stickmenAngles[i].UpdateAngles(bodies[i]);
            }
        }
    }
}
