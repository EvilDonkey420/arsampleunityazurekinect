﻿using System.Collections.Generic;
using UnityEngine;
using Avatar = LightBuzz.Kinect4Azure.Avateering.Avatar;


namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_Avateering : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private UniformImage image;
        [SerializeField] private Avatar[] avatars;

        private KinectSensor sensor;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();
        }

        private void Update()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                if (frame.ColorFrameSource != null)
                {
                    image.Load(frame.ColorFrameSource);
                }

                if (frame.BodyFrameSource != null)
                {
                    UpdateAvatars(frame.BodyFrameSource.Bodies);
                }
            }
        }

        private void OnDestroy()
        {
            sensor?.Close();
        }

        public void DoTPose()
        {
            foreach (Avatar item in avatars)
            {
                item.DoTPose();
            }
        }

        private void UpdateAvatars(IList<Body> bodies)
        {
            if (bodies == null || bodies.Count == 0) return;
            if (avatars == null || avatars.Length == 0) return;

            Body body = bodies.Closest();

            foreach (Avatar avatar in avatars)
            {
                avatar.Update(body);

                image.FlipHorizontally = avatar.Flip;
            }
        }
    }
}
