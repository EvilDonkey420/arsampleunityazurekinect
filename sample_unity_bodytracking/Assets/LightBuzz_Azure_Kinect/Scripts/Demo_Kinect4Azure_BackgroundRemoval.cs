﻿using System.Collections.Generic;
using Microsoft.Azure.Kinect.Sensor;
using UnityEngine;
using UnityEngine.UI;


namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_BackgroundRemoval : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private UniformImage image;
        [SerializeField] private GameObject stickmanPrefab;
        [SerializeField] private Transform canvas;
        
        private KinectSensor sensor;
        private List<Stickman> stickmen;
        private Color32[] colors;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();
        }

        private void Update()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                if (frame.ColorFrameSource != null && frame.UserFrameSource != null &&
                    frame.ColorFrameSource.PointCloud != null && frame.UserFrameSource.Data != null)
                {
                    BGRA[] colorData = frame.ColorFrameSource.PointCloud;
                    byte[] userData = frame.UserFrameSource.Data;

                    int width = frame.UserFrameSource.Width;
                    int height = frame.UserFrameSource.Height;
                    int size = width * height;

                    if (colors == null) colors = new Color32[size];

                    if (colorData != null && userData != null)
                    {
                        for (int i = 0; i < size; i++)
                        {
                            if (userData[i] != UserFrameSource.Background)
                            {
                                colors[i].b = colorData[i].B;
                                colors[i].g = colorData[i].G;
                                colors[i].r = colorData[i].R;
                                colors[i].a = colorData[i].A;
                            }
                            else
                            {
                                colors[i] = Color.green;
                            }
                        }

                        image.Load(colors, width, height);
                    }
                }

                if (frame.BodyFrameSource != null)
                {
                    UpdateStickmen(frame.BodyFrameSource.Bodies);
                }
            }
        }

        private void OnDestroy()
        {
            sensor?.Close();
        }

        private void UpdateStickmen(List<Body> bodies)
        {
            if (bodies == null)
            {
                return;
            }

            if (stickmen == null)
            {
                stickmen = new List<Stickman>();
            }

            if (stickmen.Count != bodies.Count)
            {
                foreach (Stickman stickman in stickmen)
                {
                    Destroy(stickman.gameObject);
                }

                stickmen.Clear();

                foreach (Body body in bodies)
                {
                    Stickman stickman = Instantiate(stickmanPrefab, canvas).GetComponent<Stickman>();
                    stickmen.Add(stickman);
                }
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                stickmen[i].UpdateStickman(bodies[i], image, Space.Depth);
            }
        }
    }
}
