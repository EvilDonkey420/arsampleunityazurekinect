﻿using System.Collections.Generic;
using UnityEngine;


namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_Depth : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] [Range(1000, 10000)] private ushort maxDepth = 8000;
        [SerializeField] private DepthVisualization depthVisualization = DepthVisualization.Grayscale;
        [SerializeField] private UniformImage image;
        [SerializeField] private GameObject stickmanPrefab;
        [SerializeField] private Transform canvas;

        private KinectSensor sensor;
        private List<Stickman> stickmen;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();
        }

        private void Update()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                if (frame.DepthFrameSource != null)
                {
                    image.Load(frame.DepthFrameSource, maxDepth, depthVisualization);
                }

                if (frame.BodyFrameSource != null)
                {
                    UpdateStickmen(frame.BodyFrameSource.Bodies);
                }
            }
        }

        private void OnDestroy()
        {
            sensor?.Close();
        }

        private void UpdateStickmen(List<Body> bodies)
        {
            if (bodies == null)
            {
                return;
            }

            if (stickmen == null)
            {
                stickmen = new List<Stickman>();
            }

            if (stickmen.Count != bodies.Count)
            {
                foreach (Stickman stickman in stickmen)
                {
                    Destroy(stickman.gameObject);
                }

                stickmen.Clear();

                foreach (Body body in bodies)
                {
                    Stickman stickman = Instantiate(stickmanPrefab, canvas).GetComponent<Stickman>();
                    stickmen.Add(stickman);
                }
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                stickmen[i].UpdateStickman(bodies[i], image, Space.Depth);
            }
        }
    }
}
