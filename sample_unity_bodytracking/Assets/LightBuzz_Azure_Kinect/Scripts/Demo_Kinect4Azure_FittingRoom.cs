﻿using LightBuzz.Kinect4Azure.Avateering;
using System.Collections.Generic;
using UnityEngine;
using Avatar = LightBuzz.Kinect4Azure.Avateering.Avatar;

namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_FittingRoom : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private UniformImage image;
        [SerializeField] private GameObject stickmanPrefab;
        [SerializeField] private Transform canvas;
        [SerializeField] private Avatar[] avatars;

        private KinectSensor sensor;
        private List<Stickman> stickmen;

        [Range(0.5f, 2f)]
        [SerializeField] private float scaleModifier = 1f;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();
        }

        private void Update()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                if (frame.ColorFrameSource != null)
                    image.Load(frame.ColorFrameSource);

                if (frame.BodyFrameSource != null)
                {
                    UpdateStickmen(frame.BodyFrameSource.Bodies);

                    Body body = frame.BodyFrameSource.Bodies.Closest();
                    UpdateAvatar(body);
                }
            }
        }

        private void UpdateAvatar(Body body)
        {
            if (body == null) return;

            Vector3 stickmanDist = stickmen[0].pointTransforms[(int)JointType.Neck].transform.position - stickmen[0].pointTransforms[(int)JointType.Pelvis].transform.position;

            Vector3 centerPos = Stickman.GetPositionOfJointInImage(body, image, Space.Color, JointType.Pelvis);
            centerPos *= image.transform.lossyScale.x;
            centerPos.x *= -1f;
            centerPos += image.transform.position;

            foreach (Avatar item in avatars)
            {
                item.Update(body);

                foreach (Avatar avatar in avatars)
                {
                    avatar.PositionBonesAtPoint(centerPos);

                    Bone b1 = avatar.GetBone(HumanBodyBones.Neck);
                    Bone b2 = avatar.GetBone(HumanBodyBones.Hips);

                    if (b1 == null || b2 == null) continue;

                    Vector3 avatarDist = b1.OriginalPosition - b2.OriginalPosition;

                    float scale = stickmanDist.magnitude / avatarDist.magnitude;
                    scale *= scaleModifier;

                    avatar.ApplyScaleAtBones(scale);
                }
            }
        }

        private void OnDestroy()
        {
            sensor?.Close();
        }

        private void UpdateStickmen(List<Body> bodies)
        {
            if (bodies == null)
            {
                return;
            }

            if (stickmen == null)
            {
                stickmen = new List<Stickman>();
            }

            if (stickmen.Count != bodies.Count)
            {
                foreach (Stickman stickman in stickmen)
                {
                    Destroy(stickman.gameObject);
                }

                stickmen.Clear();

                foreach (Body body in bodies)
                {
                    Stickman stickman = Instantiate(stickmanPrefab, canvas).GetComponent<Stickman>();
                    stickmen.Add(stickman);
                }
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                stickmen[i].UpdateStickman(bodies[i], image, Space.Color);
            }
        }
    }
}
