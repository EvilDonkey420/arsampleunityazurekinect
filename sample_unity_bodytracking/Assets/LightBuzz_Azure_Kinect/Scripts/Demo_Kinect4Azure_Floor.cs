﻿using System.Collections.Generic;
using UnityEngine;


namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_Floor : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private UniformImage image;
        [SerializeField] private Transform terrain;

        [SerializeField] private Transform bodyT;
        Dictionary<JointType, JointType> parentJointMap = new Dictionary<JointType, JointType>
        {
            { JointType.Pelvis, JointType.Head },
            { JointType.SpineNaval, JointType.Pelvis },
            { JointType.SpineChest, JointType.SpineNaval },
            { JointType.Neck, JointType.SpineChest },
            { JointType.ClavicleLeft, JointType.SpineChest },
            { JointType.ShoulderLeft, JointType.ClavicleLeft },
            { JointType.ElbowLeft, JointType.ShoulderLeft },
            { JointType.WristLeft, JointType.ElbowLeft },
            { JointType.HandLeft, JointType.WristLeft },
            { JointType.HandtipLeft, JointType.HandLeft },
            { JointType.ThumbLeft, JointType.HandLeft },
            { JointType.ClavicleRight, JointType.ClavicleRight },
            { JointType.ShoulderRight, JointType.ClavicleRight },
            { JointType.ElbowRight, JointType.ShoulderRight },
            { JointType.WristRight, JointType.ElbowRight },
            { JointType.HandRight, JointType.WristRight },
            { JointType.HandtipRight, JointType.HandRight },
            { JointType.ThumbRight, JointType.HandRight },
            { JointType.HipLeft, JointType.Pelvis },
            { JointType.KneeLeft, JointType.HipLeft },
            { JointType.AnkleLeft, JointType.KneeLeft },
            { JointType.FootLeft, JointType.AnkleLeft },
            { JointType.HipRight, JointType.Pelvis },
            { JointType.KneeRight, JointType.HipRight },
            { JointType.AnkleRight, JointType.KneeRight },
            { JointType.FootRight, JointType.AnkleRight },
            { JointType.Head, JointType.Neck },
            { JointType.Nose, JointType.Head },
            { JointType.EyeLeft, JointType.Head },
            { JointType.EarLeft, JointType.Head },
            { JointType.EyeRight, JointType.Head },
            { JointType.EarRight, JointType.Head },
        };

        private KinectSensor sensor;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();
        }

        private void Update()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                if (frame.ColorFrameSource != null)
                {
                    image.Load(frame.ColorFrameSource);
                }

                //if (frame.BodyFrameSource != null)
                //{
                //    UpdateAvatars(frame.BodyFrameSource.Bodies);
                //}

                if (frame.FloorFrameSource != null)
                {
                    UpdateFloor(frame.FloorFrameSource.Floor);
                }
            }
        }

        private void OnDestroy()
        {
            sensor?.Close();
        }

        private void UpdateAvatars(List<Body> bodies)
        {
            if (bodies == null || bodies.Count == 0) return;

            // Show joints
            Body body = bodies.Closest();

            for (int i = 0; i <= (int)JointType.EyeRight; i++)
            {
                Vector3 jointPos = body.Joints[(JointType)i].Position;
                jointPos.y *= -1f;
                UnityEngine.Quaternion jointRotation = body.Joints[(JointType)i].Orientation;

                Transform jointT = bodyT.GetChild(i);
                jointT.localPosition = jointPos;
                jointT.localRotation = jointRotation;

                Transform jointBoneT = jointT.GetChild(0);
                if (parentJointMap[(JointType)i] != JointType.Head)
                {
                    // set bone
                    Vector3 jointParentPos = body.Joints[parentJointMap[(JointType)i]].Position;
                    jointParentPos.y *= -1f;
                    Vector3 boneDirection = jointPos - jointParentPos;
                    Vector3 boneDirectionLocalSpace = Quaternion.Inverse(jointT.rotation) * Vector3.Normalize(boneDirection);

                    jointBoneT.position = jointPos - 0.5f * boneDirection;
                    jointBoneT.localRotation = Quaternion.FromToRotation(Vector3.up, boneDirectionLocalSpace);
                    jointBoneT.localScale = new Vector3(1f, 20f * 0.5f * boneDirection.magnitude, 1f);
                }
                else
                {
                    jointBoneT.gameObject.SetActive(false);
                }
            }
        }

        private void UpdateFloor(Floor floor)
        {
            //var q = Quaternion.LookRotation(new Vector3(floor.X, floor.Y, floor.Z));
            //q.EulerAngles = new Vector3(q.eulerAngles.x, q.eulerAngles.y, q.eulerAngles.z);

            terrain.position = new Vector3(0f, -floor.Height, 0f);
            terrain.rotation = UnityEngine.Quaternion.LookRotation(new Vector3(floor.X, floor.Y, floor.Z));
        }
    }
}
