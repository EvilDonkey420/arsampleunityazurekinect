﻿using UnityEngine;
using UnityEngine.UI;

namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_IMU : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private Text temperatureText;

        [SerializeField] private Transform sensorModel;

        [SerializeField] private Transform accelerometerGizmo;
        [SerializeField] private Transform gyroscopeGizmo;

        [SerializeField] private Text pitchText;
        [SerializeField] private Text yawText;
        [SerializeField] private Text rollText;

        private KinectSensor sensor;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();
        }

        private void OnDestroy()
        {
            sensor?.Close();
        }

        private void Update()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                UpdateTemperature(frame.IMUFrameSource.Temperature);

                IMU accelerometer = frame.IMUFrameSource.Accelerometer;
                IMU gyroscope = frame.IMUFrameSource.Gyroscope;

                UpdateAccelerometerGizmo(accelerometer);
                UpdateGyroscopeGizmo(gyroscope);

                UpdateKinectRotation(accelerometer);
            }
        }

        private void UpdateGyroscopeGizmo(IMU imu)
        {
            gyroscopeGizmo.transform.rotation = Quaternion.Euler(-imu.Pitch(), imu.Yaw(), imu.Roll());
        }

        private void UpdateAccelerometerGizmo(IMU imu)
        {
            accelerometerGizmo.transform.rotation = Quaternion.Euler(-imu.Pitch(), imu.Yaw(), imu.Roll());
        }

        private void UpdateKinectRotation(IMU imu)
        {
            sensorModel.transform.rotation = Quaternion.Euler(-imu.Pitch(), imu.Yaw(), imu.Roll());

            pitchText.text = "Pitch: " + imu.Pitch().ToString("N0") + "°";
            yawText.text = "Yaw: " + imu.Yaw().ToString("N0") + "°";
            rollText.text = "Roll: " + imu.Roll().ToString("N0") + "°";
        }

        private void UpdateTemperature(float celcius)
        {
            float fahrenheit = celcius * 1.8f + 32.0f;

            temperatureText.text = "Temperature: " + celcius.ToString("N0") + "°C (" + fahrenheit.ToString("N0") + "°F)";
        }
    }
}
