﻿using UnityEngine;

namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_PointCloud : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private PointCloud pointCloud;
        [SerializeField] private Transform meshParentT;
        [SerializeField] private GameObject canvasUI;

        private const float rotationSpeed = 30f;
        private const float mouseWheelScale = 0.1f;

        private KinectSensor sensor;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();

            pointCloud.Init(
                sensor.CoordinateMapper.Calibration.DepthCameraCalibration.ResolutionWidth, 
                sensor.CoordinateMapper.Calibration.DepthCameraCalibration.ResolutionHeight);
        }

        private void Update()
        {
            CheckInput();
            CheckSensor();
        }

        private void CheckInput()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                RotatePoints(-1);

                RemoveInstructions();
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                RotatePoints(1);

                RemoveInstructions();
            }
            if (Input.mouseScrollDelta.y != 0f)
            {
                Vector3 position = Camera.main.transform.localPosition;
                Camera.main.transform.localPosition = new Vector3(position.x, position.y, position.z + (Input.mouseScrollDelta.y * mouseWheelScale));
            }
        }

        private void RotatePoints(int direction)
        {
            Vector3 rotation = Camera.main.transform.localRotation.eulerAngles;
            Camera.main.transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime * direction);
        }

        private void RemoveInstructions()
        {
            canvasUI.SetActive(false);
        }

        private void CheckSensor()
        {
            if (sensor == null || !sensor.IsOpen) return;

            Frame frame = sensor.Update();

            if (frame != null)
            {
                var pointCloudDepth = frame.DepthFrameSource?.PointCloud;
                var pointCloudColor = frame.ColorFrameSource?.PointCloud;

                pointCloud.Load(pointCloudColor, pointCloudDepth);
            }
        }

        private void OnDestroy()
        {
            sensor?.Close();
        }
    }
}
