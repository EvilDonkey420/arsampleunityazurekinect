﻿using LightBuzz.Kinect4Azure.Video;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace LightBuzz.Kinect4Azure
{
    public class Demo_Kinect4Azure_Video : MonoBehaviour
    {
        [SerializeField] private Configuration configuration;
        [SerializeField] private UniformImage image;
        [SerializeField] private GameObject stickmanPrefab;
        [SerializeField] private Transform stickmanRoot;

        [SerializeField] private Image startStopBtnImg;
        [SerializeField] private Sprite[] startStopSprites;
        private bool isRecording = false;

        [SerializeField] private MediaBarPlayer mediaBarPlayer;
        [SerializeField] private GameObject recordingPanel;
        [SerializeField] private GameObject backBtnGO;

        private bool stopPlayback = false;
        private bool showSavingPanel = false;
        [SerializeField] private GameObject savingPanelGO;

        private KinectSensor sensor;
        private List<Stickman> stickmen;

        private VideoRecorder recorder;

        private void Start()
        {
            sensor = KinectSensor.Create(configuration);

            if (sensor == null)
            {
                Debug.LogError("Sensor not connected!");
                return;
            }

            sensor.Open();

            recorder = new VideoRecorder(new VideoConfiguration
            {
                Path = Path.Combine(Application.persistentDataPath, "Video"),
                ColorResolution = sensor.Configuration.ColorResolution.Size(),
                DepthResolution = sensor.Configuration.DepthMode.Size(),
                RecordColor = true,
                RecordDepth = false,
                RecordBody = true,
                RecordFloor = false,
                RecordIMU = false
            });

            recorder.OnRecordingStarted += OnRecordingStarted;
            recorder.OnRecordingStopped += OnRecordingStopped;
            recorder.OnRecordingCompleted += OnRecordingCompleted;

            Debug.Log("Video will be saved at " + recorder.Configuration.Path);
        }

        private void OnRecordingCompleted()
        {
            Debug.Log("Recording completed");

            showSavingPanel = false;
        }

        private void OnRecordingStopped()
        {
            Debug.Log("Recording stopped");

            showSavingPanel = true;
        }

        private void OnRecordingStarted()
        {
            Debug.Log("Recording started");

            stopPlayback = true;
        }

        private void Update()
        {
            if (stopPlayback)
            {
                mediaBarPlayer.Stop();

                stopPlayback = false;
            }

            if (savingPanelGO.activeSelf != showSavingPanel)
            {
                savingPanelGO.SetActive(showSavingPanel);

                // Playback
                if (!showSavingPanel)
                {
                    mediaBarPlayer.LoadVideo(recorder.Configuration.Path);
                    mediaBarPlayer.Play();
                    backBtnGO.SetActive(true);
                    recordingPanel.SetActive(false);
                }
            }

            UpdateFrame();
        }

        private void OnDestroy()
        {
            sensor?.Close();

            recorder.OnRecordingStarted -= OnRecordingStarted;
            recorder.OnRecordingStopped -= OnRecordingStopped;
            recorder.OnRecordingCompleted -= OnRecordingCompleted;
            recorder.Dispose();

            mediaBarPlayer.Dispose();
        }

        private void UpdateFrame()
        {
            Frame frame = null;

            if (mediaBarPlayer.IsPlaying)
            {
                frame = mediaBarPlayer.Update();
            }
            else if (sensor != null && sensor.IsOpen)
            {
                frame = sensor.Update();
            }

            if (frame != null)
            {
                if (frame.ColorFrameSource != null)
                {
                    image.Load(frame.ColorFrameSource);
                }

                if (frame.BodyFrameSource != null)
                {
                    UpdateStickmen(frame.BodyFrameSource.Bodies);
                }
            }

            recorder?.Update(frame);
        }

        public void BackToRecording()
        {
            mediaBarPlayer.Stop();
            recordingPanel.SetActive(true);
            backBtnGO.SetActive(false);
        }

        public void StartStopRecording()
        {
            isRecording = !isRecording;
            startStopBtnImg.sprite = startStopSprites[isRecording ? 1 : 0];

            if (isRecording)
                recorder.Start();
            else
                recorder.Stop();
        }

        private void UpdateStickmen(List<Body> bodies)
        {
            if (bodies == null)
            {
                return;
            }

            if (stickmen == null)
            {
                stickmen = new List<Stickman>();
            }

            if (stickmen.Count != bodies.Count)
            {
                foreach (Stickman stickman in stickmen)
                {
                    Destroy(stickman.gameObject);
                }

                stickmen.Clear();

                foreach (Body body in bodies)
                {
                    Stickman stickman = Instantiate(stickmanPrefab, stickmanRoot).GetComponent<Stickman>();
                    stickmen.Add(stickman);
                }
            }

            for (int i = 0; i < bodies.Count; i++)
            {
                stickmen[i].UpdateStickman(bodies[i], image, Space.Color);
            }
        }
    }
}
