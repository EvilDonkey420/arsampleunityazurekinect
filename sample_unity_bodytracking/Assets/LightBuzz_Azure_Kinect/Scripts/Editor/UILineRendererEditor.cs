﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using LightBuzz.Kinect4Azure;

namespace LightBuzz.Kinect4Azure_Editor
{
    [CustomEditor(typeof(UILineRenderer))]
    public class UILineRendererEditor : Editor
    {
        new UILineRenderer target;
        bool showPositionHandles;

        void OnEnable()
        {
            target = (UILineRenderer)base.target;

            Undo.undoRedoPerformed = OnUndoRedo;
        }

        void OnDisable()
        {
            Undo.undoRedoPerformed = null;
        }

        public override void OnInspectorGUI()
        {
            Undo.RecordObject(target, "Undo UILineRenderer");

            target.lineWidth = EditorGUILayout.FloatField("Line Width", target.lineWidth);
            target.canvasRenderer = (CanvasRenderer)EditorGUILayout.ObjectField("Canvas Renderer", target.canvasRenderer, typeof(CanvasRenderer), true);
            target.lineMaterial = (Material)EditorGUILayout.ObjectField("Line Material", target.lineMaterial, typeof(Material), true);
            showPositionHandles = EditorGUILayout.Toggle("Show Position Handles", showPositionHandles);

            target.positionsFoldOut = EditorGUILayout.Foldout(target.positionsFoldOut, "Positions", true);

            if (target.positionsFoldOut)
            {
                EditorGUI.indentLevel = 1;

                int count = Mathf.Max(2, EditorGUILayout.IntField("Size", target.PositionCount));
                target.PositionCount = count;

                for (int i = 0; i < count; i++)
                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("Position " + i, GUILayout.Width(100));

                    GUILayout.FlexibleSpace();
                    GUILayout.Space(-2);

                    Vector2 pos = target.GetPosition(i);

                    EditorGUILayout.BeginHorizontal();
                    pos.x = FloatField("X", pos.x);
                    EditorGUILayout.EndHorizontal();

                    GUILayout.Space(-19);
                    GUILayout.FlexibleSpace();

                    EditorGUILayout.BeginHorizontal();
                    pos.y = FloatField("Y", pos.y);
                    EditorGUILayout.EndHorizontal();
                    target.SetPosition(i, pos);

                    GUILayout.Space(-19);
                    GUILayout.FlexibleSpace();

                    EditorGUILayout.BeginHorizontal();
                    target.SetSize(i, FloatField("S", target.GetSize(i)));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.EndHorizontal();

                    if (i + 1 == count)
                    {
                        EditorGUILayout.Space();
                    }
                }
            }

            if (GUI.changed)
            {
                Undo.RecordObject(target, "Undo UILineRenderer");
                EditorUtility.SetDirty(target);
                target.RefreshMeshData();
            }
        }

        void OnSceneGUI()
        {
            Undo.RecordObject(target, "Undo UILineRenderer");

            if (showPositionHandles)
            {
                Color tempColor = Handles.color;
                Tools.current = Tool.View;

                for (int i = 0; i < target.PositionCount; i++)
                {
                    // Get World Position
                    Vector3 worldPos = target.transform.position + new Vector3(target.GetPosition(i).x, target.GetPosition(i).y, 0);

                    // Draw Position Handle
                    Vector3 handlePosition = Handles.PositionHandle(worldPos, UnityEngine.Quaternion.identity);
                    handlePosition -= target.transform.position;

                    // Set Local Position
                    target.SetPosition(i, new Vector2(handlePosition.x, handlePosition.y));

                    Handles.color = Color.blue;
                    // Draw Scale Handle
                    target.SetSize(i, Handles.ScaleSlider(target.GetSize(i), worldPos, Vector3.left, UnityEngine.Quaternion.identity, 0.4f, 0.5f));
                }

                // GUI Buttons
                Handles.BeginGUI();
                Vector3 lastPos = Camera.current.WorldToScreenPoint(target.transform.position + new Vector3(target.GetPosition(target.PositionCount - 1).x, target.GetPosition(target.PositionCount - 1).y, 0));

                if (target.PositionCount > 2 && GUI.Button(new Rect(lastPos.x + 30, Screen.height - lastPos.y, 60, 25), "Remove"))
                {
                    target.RemovePosition(target.PositionCount - 1);
                    target.ReconstructMeshData();
                }

                if (GUI.Button(new Rect(lastPos.x - 30, Screen.height - lastPos.y, 60, 25), "Add"))
                {
                    Vector2 newPos = target.GetPosition(target.PositionCount - 1) + (target.GetPosition(target.PositionCount - 1) - target.GetPosition(target.PositionCount - 2));

                    target.AddNewPosition(newPos, target.GetSize(target.PositionCount - 1));
                    target.ReconstructMeshData();
                }
                Handles.EndGUI();

                // Add and remove with key
                // BackSlash/Pipe Adds  (under BackSpace Key)
                // BackSpace Removes
                Event e = Event.current;
                if (e.isKey && e.type == EventType.KeyUp)
                {
                    switch (e.keyCode)
                    {
                        case KeyCode.Backspace:
                            target.RemovePosition(target.PositionCount - 1);
                            target.ReconstructMeshData();
                            break;

                        case KeyCode.Backslash:
                            target.AddNewPosition(target.GetPosition(target.PositionCount - 1) + (target.GetPosition(target.PositionCount - 1) - target.GetPosition(target.PositionCount - 2)), target.GetSize(target.PositionCount - 1));
                            target.ReconstructMeshData();
                            break;
                    }
                }

                if (GUI.changed)
                {
                    Undo.RecordObject(target, "Undo UILineRenderer");
                    EditorUtility.SetDirty(target);
                    target.RefreshMeshData();
                }

                Handles.color = tempColor;
            }
        }

        float FloatField(string label, float value, int width = 30)
        {
            float temp = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = width;
            value = EditorGUILayout.FloatField(label, value);
            EditorGUIUtility.labelWidth = temp;

            return value;
        }

        void OnUndoRedo()
        {
            if (target != null)
            {
                target.ReconstructMeshData();
            }
        }

        [MenuItem("LightBuzz/UI LineRenderer/Create")]
        public static void Create()
        {
            Canvas canvas = FindObjectOfType<Canvas>();
            if (canvas == null)
            {
                GameObject canvasObject = new GameObject("Canvas");
                canvas = canvasObject.AddComponent<Canvas>();
                canvas.renderMode = RenderMode.ScreenSpaceOverlay;
                canvas.pixelPerfect = false;
                canvas.sortingOrder = 0;
                canvas.targetDisplay = 0;
                canvas.additionalShaderChannels = AdditionalCanvasShaderChannels.None;

                CanvasScaler scaler = canvasObject.AddComponent<CanvasScaler>();
                scaler.uiScaleMode = CanvasScaler.ScaleMode.ConstantPixelSize;
                scaler.scaleFactor = 1;
                scaler.referencePixelsPerUnit = 100;

                GraphicRaycaster raycaster = canvasObject.AddComponent<GraphicRaycaster>();
                raycaster.ignoreReversedGraphics = true;
                raycaster.blockingObjects = GraphicRaycaster.BlockingObjects.None;
            }

            EventSystem eventSystem = FindObjectOfType<EventSystem>();
            if (eventSystem == null)
            {
                GameObject eventSystemObject = new GameObject("EventSystem");
                eventSystem = eventSystemObject.AddComponent<EventSystem>();
                eventSystem.firstSelectedGameObject = null;
                eventSystem.sendNavigationEvents = true;
                eventSystem.pixelDragThreshold = 5;

                StandaloneInputModule sim = eventSystemObject.AddComponent<StandaloneInputModule>();
                sim.horizontalAxis = "Horizontal";
                sim.verticalAxis = "Vertical";
                sim.submitButton = "Submit";
                sim.cancelButton = "Cancel";
                sim.inputActionsPerSecond = 10;
                sim.repeatDelay = 0.5f;
                sim.forceModuleActive = false;
            }

            GameObject newLineObject = new GameObject("Line", typeof(RectTransform));
            UILineRenderer line = newLineObject.AddComponent<UILineRenderer>();
            line.canvasRenderer = newLineObject.GetComponent<CanvasRenderer>();
            line.lineMaterial = new Material(Shader.Find("UI/Default"));
            line.AddRangeOfPositions(2, 10, Color.white);
            line.SetPosition(0, Vector2.zero);
            line.SetPosition(1, Vector2.right * 100);
            line.RefreshMeshData();

            newLineObject.transform.SetParent(canvas.transform, false);
        }
    }
}
