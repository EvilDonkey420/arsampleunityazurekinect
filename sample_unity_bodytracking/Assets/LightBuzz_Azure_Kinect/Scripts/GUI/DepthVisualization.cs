﻿namespace LightBuzz.Kinect4Azure
{
    public enum DepthVisualization
    {
        Grayscale,
        Jet
    }
}