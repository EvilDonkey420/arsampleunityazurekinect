﻿using Microsoft.Azure.Kinect.Sensor;
using UnityEngine;

public class PointCloud : MonoBehaviour
{
    private Mesh mesh;
    private int size;
    private int[] indices;
    private Vector3[] vertices;
    private Color32[] colors;

    public void Init(int width, int height)
    {
        if (width == 0 || height == 0)
        {
            Debug.LogError("You need to set the width and height of the depth frame.");
            return;
        }

        size = width * height;

        mesh = new Mesh { indexFormat = UnityEngine.Rendering.IndexFormat.UInt32 };

        vertices = new Vector3[size];
        colors = new Color32[size];
        indices = new int[size];

        for (int i = 0; i < size; i++)
        {
            indices[i] = i;
        }

        mesh.vertices = vertices;
        mesh.colors32 = colors;
        mesh.SetIndices(indices, MeshTopology.Points, 0);

        gameObject.GetComponent<MeshFilter>().mesh = mesh;
    }

    public void Load(BGRA[] pointCloudColor, Short3[] pointCloudDepth)
    {
        for (int i = 0; i < size; i++)
        {
            if (pointCloudDepth != null)
            {
                vertices[i].x = pointCloudDepth[i].X / 1000.0f;
                vertices[i].y = -pointCloudDepth[i].Y / 1000.0f;
                vertices[i].z = pointCloudDepth[i].Z / 1000.0f;
            }

            if (pointCloudColor != null)
            {
                colors[i].b = pointCloudColor[i].B;
                colors[i].g = pointCloudColor[i].G;
                colors[i].r = pointCloudColor[i].R;
                colors[i].a = pointCloudColor[i].A;
            }
        }

        mesh.vertices = vertices;
        mesh.colors32 = colors;
        mesh.RecalculateBounds();
    }
}
