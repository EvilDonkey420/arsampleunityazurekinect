﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace LightBuzz.Kinect4Azure
{
    public enum Space
    {
        Color,
        Depth
    }

    public class Stickman : MonoBehaviour
    {
        protected readonly int JOINT_TYPE_COUNT = Enum.GetValues(typeof(JointType)).Length;

        protected readonly List<Tuple<JointType, JointType>> pairs = new List<Tuple<JointType, JointType>>
    {
        Tuple.Create(JointType.Head, JointType.Neck),
        Tuple.Create(JointType.Neck, JointType.SpineChest),
        Tuple.Create(JointType.SpineChest, JointType.SpineNaval),
        Tuple.Create(JointType.SpineNaval, JointType.Pelvis),
        Tuple.Create(JointType.Pelvis, JointType.HipLeft),
        Tuple.Create(JointType.Pelvis, JointType.HipRight),
        Tuple.Create(JointType.ClavicleLeft, JointType.ShoulderLeft),
        Tuple.Create(JointType.ClavicleRight, JointType.ShoulderRight),
        Tuple.Create(JointType.ShoulderLeft, JointType.ElbowLeft),
        Tuple.Create(JointType.ShoulderRight, JointType.ElbowRight),
        Tuple.Create(JointType.ElbowLeft, JointType.WristLeft),
        Tuple.Create(JointType.ElbowRight, JointType.WristRight),
        Tuple.Create(JointType.WristLeft, JointType.HandLeft),
        Tuple.Create(JointType.WristRight, JointType.HandRight),
        Tuple.Create(JointType.HandLeft, JointType.HandtipLeft),
        Tuple.Create(JointType.HandRight, JointType.HandtipRight),
        Tuple.Create(JointType.HandLeft, JointType.ThumbLeft),
        Tuple.Create(JointType.HandRight, JointType.ThumbRight),
        Tuple.Create(JointType.HipLeft, JointType.KneeLeft),
        Tuple.Create(JointType.HipRight, JointType.KneeRight),
        Tuple.Create(JointType.KneeLeft, JointType.AnkleLeft),
        Tuple.Create(JointType.KneeRight, JointType.AnkleRight),
        Tuple.Create(JointType.AnkleLeft, JointType.FootLeft),
        Tuple.Create(JointType.AnkleRight, JointType.FootRight)
    };

        public Transform[] pointTransforms = null;
        public Transform[] lineTransforms = null;
        UILineRenderer[] uiLines = null;

        bool initialized = false;

        bool show = true;

        void Initialize(UniformImage image)
        {
            int jointLineCount = pairs.Count;

            uiLines = new UILineRenderer[jointLineCount];

            for (int i = 0; i < jointLineCount; i++)
            {
                uiLines[i] = lineTransforms[i].GetComponent<UILineRenderer>();
            }

            initialized = true;
        }

        public void UpdateStickman(Body body, UniformImage image, Space space = Space.Color)
        {
            if (!initialized)
            {
                Initialize(image);
            }

            if (body == null)
            {
                Hide();

                return;
            }

            Show();

            for (int i = 0; i < JOINT_TYPE_COUNT; i++)
            {
                if (body.Joints[(JointType)i].TrackingState != TrackingState.None)
                {
                    if (!pointTransforms[i].gameObject.activeSelf)
                    {
                        pointTransforms[i].gameObject.SetActive(true);
                    }

                    ((RectTransform)pointTransforms[i]).anchoredPosition = GetPositionOfJointInImage(body, image, space, (JointType)i);
                }
                else
                {
                    if (pointTransforms[i].gameObject.activeSelf)
                    {
                        pointTransforms[i].gameObject.SetActive(false);
                    }
                }
            }

            for (int i = 0; i < pairs.Count; i++)
            {
                UpdateLine(i, body, pairs[i].Item1, pairs[i].Item2);
            }
        }

        /// <summary>
        /// Calculates the local position in the image of a joint.
        /// </summary>
        /// <param name="body"></param>
        /// <param name="image"></param>
        /// <param name="sensor"></param>
        /// <param name="jointType"></param>
        /// <returns></returns>
        public static Vector3 GetPositionOfJointInImage(Body body, UniformImage image, Space space, JointType jointType)
        {
            LightBuzz.Vector2D point = space == Space.Color ?
                    body.Joints[jointType].PositionColor :
                    body.Joints[jointType].PositionDepth;
            Vector2 position = new Vector2(point.X, point.Y);

            return image.GetPosition(position);
        }

        public void Hide()
        {
            Toggle(false);
        }

        public void Show()
        {
            Toggle(true);
        }

        public void Toggle(bool display)
        {
            if (show == display) return;

            show = display;

            foreach (UILineRenderer item in uiLines)
                item.enabled = show;

            foreach (Transform item in pointTransforms)
                item.gameObject.SetActive(show);
        }

        void UpdateLine(int index, Body body, JointType type1, JointType type2)
        {
            if (body.Joints[type1].TrackingState != TrackingState.None && body.Joints[type2].TrackingState != TrackingState.None)
            {
                Vector2 p1 = ((RectTransform)pointTransforms[(int)type1]).anchoredPosition;
                Vector2 p2 = ((RectTransform)pointTransforms[(int)type2]).anchoredPosition;

                uiLines[index].enabled = true;

                try
                {
                    uiLines[index].SetPosition(0, p1);
                    uiLines[index].SetPosition(1, p2);
                    uiLines[index].RefreshMeshData();
                }
                catch
                {
                }
            }
            else
            {
                uiLines[index].enabled = false;
            }
        }
    }
}
