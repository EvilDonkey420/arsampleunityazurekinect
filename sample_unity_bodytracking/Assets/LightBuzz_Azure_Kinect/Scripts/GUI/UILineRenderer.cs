﻿using System.Collections.Generic;
using UnityEngine;


namespace LightBuzz.Kinect4Azure
{
    [ExecuteInEditMode, RequireComponent(typeof(CanvasRenderer))]
    public class UILineRenderer : MonoBehaviour
    {
        #region Variables and Properties

#if UNITY_EDITOR
        public bool positionsFoldOut = false;
        public bool showHandles = true;
        public bool lockLine = false;
#endif

        public float lineWidth = 1;
        public bool worldSpace = false;

        public CanvasRenderer canvasRenderer = null;
        public Material lineMaterial = null;

        [SerializeField]
        Vector3[] vertices;
        [SerializeField]
        Vector2[] uvs;
        [SerializeField]
        int[] triangles;
        [SerializeField]
        Color32[] vertexColors;

        [SerializeField]
        Mesh mesh;
        public Mesh Mesh { get { return mesh; } }

        #endregion

        #region Variables with Properties

        [SerializeField]
        List<Vector2> positions = new List<Vector2>();
        [SerializeField]
        List<float> sizes = new List<float>();
        [SerializeField]
        List<Color32> colors = new List<Color32>();
        public int PositionCount
        {
            get
            {
                return positions.Count;
            }
            set
            {
                value = Mathf.Max(2, value);

                if (positions.Count != value)
                {
                    int posCount = positions.Count;

                    if (value > posCount)
                    {
                        AddRangeOfPositions(value - posCount);
                    }
                    else
                    {
                        RemoveRangeOfPositions(value, posCount - value);
                    }
                }
            }
        }

        #endregion

        #region Reserved methods // Awake - OnEnable - OnDisable - OnDestroy - Update

        void Awake()
        {
            ReconstructMeshData();
        }

        void OnEnable()
        {
            if (canvasRenderer == null) return;

            canvasRenderer.cull = false;
        }

        void OnDisable()
        {
            if (canvasRenderer == null) return;

            canvasRenderer.cull = true;
        }

        void OnDestroy()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                DestroyImmediate(mesh);
            }
            else
#endif
                Destroy(mesh);
        }

        void Update()
        {
            if (worldSpace && transform.hasChanged)
            {
                RefreshMeshData();
            }
        }

        #endregion

        #region ReconstructMeshData

        /// <summary>
        /// Call it only after you add or remove one or more positions
        /// </summary>
        public void ReconstructMeshData()
        {
            if (mesh == null)
            {
                mesh = new Mesh();
            }

            mesh.Clear();

            if (PositionCount >= 0)
            {
                vertices = new Vector3[2 * PositionCount + 2];
                vertexColors = new Color32[vertices.Length];
                uvs = new Vector2[vertices.Length];
                triangles = new int[PositionCount * 6];

                Internal_RefreshMeshData();
            }
            else
            {
                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
            }
        }

        #endregion

        #region RefreshMeshData

        /// <summary>
        /// Call it after you make one or more changes with SetPosition or SetSize
        /// </summary>
        public void RefreshMeshData()
        {
            if (mesh == null)
            {
                mesh = new Mesh();
                if (GetComponent<MeshFilter>() != null)
                {
                    GetComponent<MeshFilter>().sharedMesh = mesh;
                }
            }

            Internal_RefreshMeshData();
        }

        void Internal_RefreshMeshData()
        {
            int positionCount = PositionCount;

            if (positionCount <= 1)
            {
                mesh.Clear(true);
            }
            else
            {
                #region Vertices

                Vector2 worldPosition = worldSpace ? (Vector2)transform.position : Vector2.zero;

                for (int i = 0, ii = 0, c = positionCount - 1; i < c; i++)
                {
                    Vector2 curr = positions[i];
                    Vector2 next = positions[i + 1];

                    Vector2 sides = (next - curr).normalized * (lineWidth * sizes[i]) * 0.5f;
                    sides.Set(-sides.y, sides.x);

                    float angle;
                    bool fixWidth = false;

                    if (i == 0)
                    {
                        vertices[0] = new Vector3(curr.x + sides.x - worldPosition.x, curr.y + sides.y - worldPosition.y, 0);
                        vertices[1] = new Vector3(curr.x - sides.x - worldPosition.x, curr.y - sides.y - worldPosition.y, 0);

                        vertexColors[0] = colors[0];
                        vertexColors[1] = colors[0];

                        if (positionCount > 2)
                        {
                            angle = Vector3.Angle((next - curr).normalized, (positions[i + 2] - next).normalized) * 0.5f;

                            if (Vector3.Dot(UnityEngine.Quaternion.Euler(0, 0, 90) * (next - curr).normalized, (positions[i + 2] - next).normalized) > 0)
                            {
                                angle += 90;
                            }
                            else
                            {
                                angle = 90 - angle;
                            }

                            if (angle < 160f && angle > 30f && i + 2 < positionCount)
                            {
                                fixWidth = true;
                            }
                        }
                        else
                        {
                            angle = 90;
                        }

                        if (fixWidth)
                        {
                            sides = next.GetCenterFromCorner(curr, positions[i + 2], lineWidth * sizes[i + 1] * 0.5f);
                        }
                        else
                        {
                            sides = UnityEngine.Quaternion.Euler(0, 0, angle) * ((next - curr).normalized * (lineWidth * sizes[i + 1]) * 0.5f) * 2f;
                        }

                        if (positionCount == 2)
                        {
                            sides *= 0.5f;
                        }

                        vertices[2] = new Vector3(next.x - sides.x - worldPosition.x, next.y - sides.y - worldPosition.y, 0);
                        vertices[3] = new Vector3(next.x + sides.x - worldPosition.x, next.y + sides.y - worldPosition.y, 0);

                        vertexColors[2] = colors[1];
                        vertexColors[3] = colors[1];
                    }
                    else
                    {
                        ii = i * 2;

                        if (i + 2 < positionCount)
                        {
                            angle = Vector3.Angle((next - curr).normalized, (positions[i + 2] - next).normalized) * 0.5f;

                            if (Vector3.Dot(UnityEngine.Quaternion.Euler(0, 0, 90) * (next - curr).normalized, (positions[i + 2] - next).normalized) > 0)
                            {
                                angle += 90;
                            }
                            else
                            {
                                angle = 90 - angle;
                            }

                            if (angle < 160f && angle > 30f && i + 2 < positionCount)
                            {
                                fixWidth = true;
                            }
                        }
                        else
                        {
                            angle = 90;
                        }

                        sides = UnityEngine.Quaternion.Euler(0, 0, angle) * ((next - curr).normalized * (lineWidth * sizes[i + 1]) * 0.5f);

                        if (fixWidth)
                        {
                            sides = next.GetCenterFromCorner(curr, positions[i + 2], lineWidth * sizes[i + 1] * 0.5f);
                        }

                        if ((i + 1) % 2 == 0)
                        {
                            vertices[ii + 2] = new Vector3(next.x + sides.x - worldPosition.x, next.y + sides.y - worldPosition.y, 0);
                            vertices[ii + 3] = new Vector3(next.x - sides.x - worldPosition.x, next.y - sides.y - worldPosition.y, 0);
                        }
                        else
                        {
                            vertices[ii + 2] = new Vector3(next.x - sides.x - worldPosition.x, next.y - sides.y - worldPosition.y, 0);
                            vertices[ii + 3] = new Vector3(next.x + sides.x - worldPosition.x, next.y + sides.y - worldPosition.y, 0);
                        }

                        vertexColors[ii + 2] = colors[i + 1];
                        vertexColors[ii + 3] = colors[i + 1];
                    }
                }

                #endregion

                #region UV

                float uvDelta = 1f / (positionCount - 1);

                for (int i = 0; i < positionCount; i++)
                {
                    uvs[i * 2] = new Vector2(uvDelta * i, (i + 1) % 2);
                    uvs[i * 2 + 1] = new Vector2(uvs[i * 2].x, i % 2);
                }

                #endregion

                #region Triangles

                for (int i = 0, section = 0; i < positionCount - 1; i++)
                {
                    int triangle = i * 6;
                    int vertex = i * 3 - section;

                    if (i % 2 == 0)
                    {
                        triangles[triangle] = vertex;
                        triangles[triangle + 1] = triangles[triangle] + 2;
                        triangles[triangle + 2] = triangles[triangle] + 1;
                        triangles[triangle + 3] = triangles[triangle];
                        triangles[triangle + 4] = triangles[triangle + 1] + 1;
                        triangles[triangle + 5] = triangles[triangle + 1];
                    }
                    else
                    {
                        triangles[triangle] = vertex;
                        triangles[triangle + 1] = triangles[triangle] + 1;
                        triangles[triangle + 2] = triangles[triangle] + 2;
                        triangles[triangle + 3] = triangles[triangle];
                        triangles[triangle + 4] = triangles[triangle + 2];
                        triangles[triangle + 5] = triangles[triangle] - 1;
                    }

                    if (i % 2 == 1)
                    {
                        section += 2;
                    }
                }

                #endregion

                mesh.vertices = vertices;
                mesh.uv = uvs;
                mesh.triangles = triangles;
                mesh.colors32 = vertexColors;

                mesh.RecalculateNormals();
            }

            mesh.RecalculateBounds();

            if (canvasRenderer != null)
            {
                canvasRenderer.SetMaterial(lineMaterial, null);
                canvasRenderer.SetMesh(mesh);
            }
        }

        #endregion

        #region ClearPositions

        /// <summary>
        /// Clears all the positions
        /// </summary>
        public void ClearPositions()
        {
            positions.Clear();
            sizes.Clear();
            colors.Clear();
        }

        #endregion

        #region AddNewPosition

        /// <summary>
        /// Adds a new position.
        /// </summary>
        /// <param name="position"></param>
        public void AddNewPosition(Vector2 position)
        {
            AddNewPosition(position, GetLastSize(), GetLastColor());
        }

        /// <summary>
        /// Adds a new position with the given size.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        public void AddNewPosition(Vector2 position, float size)
        {
            AddNewPosition(position, size, GetLastColor());
        }

        /// <summary>
        /// Adds a new position with the given size and color.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <param name="color"></param>
        public void AddNewPosition(Vector2 position, float size, Color32 color)
        {
            AddNewPosition(GetLastIndex() + 1, position, size, color);
        }

        /// <summary>
        /// Adds a new position.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="position"></param>
        public void AddNewPosition(int index, Vector2 position)
        {
            AddNewPosition(index, position, GetLastSize(), GetLastColor());
        }

        /// <summary>
        /// Adds a new position with the given size.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="position"></param>
        /// <param name="size"></param>
        public void AddNewPosition(int index, Vector2 position, float size)
        {
            AddNewPosition(index, position, size, GetLastColor());
        }

        /// <summary>
        /// Adds a new position with the given size and color.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <param name="color"></param>
        public void AddNewPosition(int index, Vector2 position, float size, Color32 color)
        {
            positions.Insert(index, position);
            sizes.Insert(index, size);
            colors.Insert(index, color);
        }

        #endregion

        #region AddRangeOfPositions

        /// <summary>
        /// Adds a range of positions using the last values of the line.
        /// </summary>
        /// <param name="range"></param>
        public void AddRangeOfPositions(int range)
        {
            AddRangeOfPositions(range, GetLastSize(), GetLastColor());
        }

        /// <summary>
        /// Adds a range of positions with the given size and the last color of the line.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="size"></param>
        public void AddRangeOfPositions(int range, float size)
        {
            AddRangeOfPositions(range, size, GetLastColor());
        }

        /// <summary>
        /// Adds a range of positions with the given size and color.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="size"></param>
        /// <param name="color"></param>
        public void AddRangeOfPositions(int range, float size, Color32 color)
        {
            if (range < 1) return;

            Vector2 lastPosition = GetLastPosition();

            Vector2[] positionsArr = new Vector2[range];
            for (int i = 0; i < range; i++)
            {
                positionsArr[i] = lastPosition;
            }
            positions.AddRange(positionsArr);

            for (int i = 0; i < range; i++)
            {
                sizes.Add(size);
            }

            for (int i = 0; i < range; i++)
            {
                colors.Add(color);
            }

            ReconstructMeshData();
        }

        #endregion

        #region RemovePosition

        /// <summary>
        /// Removes a position.
        /// </summary>
        /// <param name="index"></param>
        public void RemovePosition(int index)
        {
            if (index < 0) return;

            positions.RemoveAt(index);
            sizes.RemoveAt(index);
            colors.RemoveAt(index);
        }

        #endregion

        #region RemoveRangeOfPositions

        /// <summary>
        /// Removes a range of positions.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="range"></param>
        public void RemoveRangeOfPositions(int index, int range)
        {
            if (index < 0 || range < 1) return;

            positions.RemoveRange(index, range);
            sizes.RemoveRange(index, range);
            colors.RemoveRange(index, range);

            ReconstructMeshData();
        }

        #endregion

        #region SetPosition

        /// <summary>
        /// Sets a position
        /// </summary>
        /// <param name="index"></param>
        /// <param name="position"></param>
        public void SetPosition(int index, Vector2 position)
        {
            positions[index] = position;
        }

        #endregion

        #region GetPosition

        /// <summary>
        /// Gets a position
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Vector2 GetPosition(int index)
        {
            return positions[index];
        }

        #endregion

        #region SetSize

        /// <summary>
        /// Sets a size
        /// </summary>
        /// <param name="index"></param>
        /// <param name="size"></param>
        public void SetSize(int index, float size)
        {
            sizes[index] = size;
        }

        #endregion

        #region GetSize

        /// <summary>
        /// Gets a size
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public float GetSize(int index)
        {
            return sizes[index];
        }

        #endregion

        #region SetColor

        /// <summary>
        /// Sets a color
        /// </summary>
        /// <param name="index"></param>
        /// <param name="color"></param>
        public void SetColor(int index, Color32 color)
        {
            colors[index] = color;
        }

        #endregion

        #region GetColor

        /// <summary>
        /// Gets a color
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Color32 GetColor(int index)
        {
            return colors[index];
        }

        #endregion

        #region GetLastIndex

        int GetLastIndex()
        {
            return positions.Count == 0 ? 0 : positions.Count - 1;
        }

        #endregion

        #region GetLastPosition

        Vector2 GetLastPosition()
        {
            return positions.Count == 0 ? Vector2.zero : positions[positions.Count - 1];
        }

        #endregion

        #region GetLastSize

        float GetLastSize()
        {
            return sizes.Count == 0 ? 1 : sizes[sizes.Count - 1];
        }

        #endregion

        #region GetLastColor

        Color32 GetLastColor()
        {
            return colors.Count == 0 ? (Color32)Color.white : colors[colors.Count - 1];
        }

        #endregion
    }

    public static class UILineRendererExtension
    {
        public static Vector3 GetCenterFromCorner(this Vector3 center, Vector3 lhs, Vector3 rhs, float width)
        {
            Vector3 lhDir = (center - lhs).normalized;
            Vector3 tangent = ((rhs - center).normalized + lhDir).normalized;
            Vector3 miter = new Vector3(-tangent.y, tangent.x);

            return miter * (width / Vector3.Dot(miter, new Vector3(-lhDir.y, lhDir.x)));
        }

        public static Vector2 GetCenterFromCorner(this Vector2 center, Vector2 lhs, Vector2 rhs, float width)
        {
            Vector2 lhDir = (center - lhs).normalized;
            Vector2 tangent = ((rhs - center).normalized + lhDir).normalized;
            Vector2 miter = new Vector2(-tangent.y, tangent.x);

            return miter * (width / Vector2.Dot(miter, new Vector2(-lhDir.y, lhDir.x)));
        }
    }
}
